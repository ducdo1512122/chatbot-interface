require('dotenv').config();

// Required library
var express = require('express');
var app = express();
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var config = require('./configs/protocols');
var route = require('./routes/main');
var control_panel = require('./routes/bot_manager');
var socket_controller = require('./controllers/sockets/connectors');
var database_controller = require('./models/db_init');
var knowledge_base = require('./services/dialogflow/knowledge_connector');
var fulfillment = require('./services/dialogflow/fulfillments');
var fulfillmentPost = require('./services/dialogflow/fulfillmentsPost');

// App set-up
app.use(express.static(path.join(__dirname, 'public'), { dotfiles: 'allow' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Connection set-up
socket_controller.init_connection();
socket_controller.run_connection();

// database_controller.dbConnect();

// var fs = require('fs');
// var buffer = fs.readFileSync('./configs/data/data.csv');
// knowledge_base.create_knowledge_base('newagent-rifpgt', 'test');
// knowledge_base.add_document('newagent-rifpgt', process.env.KNOWLEDGE_BASE_NAME, buffer, 'test', 'FAQ', 'text/csv')
// knowledge_base.dectectTextIntentWithKnowledgeBase('your name');

fulfillment.fulfillmentConnect();

// Routes
app.use('/', route);
app.use('/controlpanel', control_panel);
app.use('/', fulfillmentPost);

// Change the 404 message modifing the middleware
app.use(function(req, res, next) {
    res.status(404).send("Sorry, that route doesn't exist. Have a nice day :)");
});

// ====================================== START SERVER ==================================================
if (process.env.MODE == "server") {
    // Init HTTPS server
    var server = require('https').Server(config.credentials, app);
    server.listen(process.env.PORT_SECURE, process.env.HOST_NAME, function() {
        console.log(`Server running at https://${process.env.HOST_NAME}:${process.env.PORT_SECURE}/`);
    });
}
else {
    // Init HTTP server
    var server = require('http').Server(app);
    server.listen(process.env.PORT, process.env.HOST_NAME, function() {
        console.log(`Server running at http://${process.env.HOST_NAME}:${process.env.PORT}/`);
    });
}