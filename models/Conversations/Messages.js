var mongoose = require("mongoose");
var Schema = mongoose.Schema;

const messageSchema = new Schema({
    message: {
        type: String
    },
    sender: {
        type: String
    }
});

const Message = mongoose.model("Message", messageSchema);

module.exports = Message;