const fs = require('fs');

if (process.env.MODE == "server") {
    // Certificate settings
    const privateKey = fs.readFileSync(process.env.PRIVATE_KEY, process.env.ENCODING);
    const certificate = fs.readFileSync(process.env.CERTIFICATE, process.env.ENCODING);
    const ca = fs.readFileSync(process.env.CA, process.env.ENCODING);
    var credentials = { 
      key: privateKey,        
      cert: certificate,        
      ca: ca
    };
}

module.exports = {
  credentials
}