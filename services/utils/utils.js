function convertJSON(data) {
    outputMessages = {}
  
    messArray = data.queryResult.fulfillmentMessages
    for (var i = 0; i < messArray.length; i++) {
      if (messArray[i].message == "text" && "text" in messArray[i]) {
        if (messArray[i].text.text[0].length > 0) {
          outputMessages.text = messArray[i].text.text; // Get text array response from API
        }
        else {
          var array_api = JSON.parse(data.queryResult.fulfillmentText);
          var array_rs = [];

          for (var apiIter = 0; apiIter < array_api.length; apiIter++) {
            array_rs.push("* <b>" + array_api[apiIter].Convention +
                          "</b>. From: " + array_api[apiIter].StartDate +
                          ". To: " + array_api[apiIter].EndDate +
                          ". At " + array_api[apiIter].Venues);
          }

          outputMessages.text = [array_rs.join('; <br>')];
        }
      }
      else if (messArray[i].message == "payload" && "payload" in messArray[i]) {
        
        qrArr = messArray[i].payload.fields.metadata.structValue.fields.payload.listValue.values
  
        if (qrArr.length > 1) {
          outputMessages.payload = {}
          outputMessages.payload.message = messArray[i].payload.fields.message.stringValue
  
          // Return array of quick replies
          var opts = []
          for (var j = 0; j < qrArr.length; j++) {
            qrValue = qrArr[j]['structValue']['fields']['name']['stringValue']
            opts.push({text: qrValue, value: qrValue});
          }
  
          // Add array of quick replies to output message
          outputMessages.payload.values = opts
        }
        else if (qrArr.length == 1) {
          fieldsRs = qrArr[0]['structValue']['fields']
  
          if ("url" in fieldsRs) {
            // Return images
            outputMessages.image = fieldsRs.url.stringValue
          }
          else if ("buttons" in fieldsRs) {
            // Return cards
            outputMessages.card = {}
  
            outputMessages.card.title = fieldsRs.buttons.listValue.values[0].structValue.fields.name.stringValue
            outputMessages.card.link = fieldsRs.buttons.listValue.values[0].structValue.fields.action.structValue.fields.payload.structValue.fields.url.stringValue
          }
        }
      }
    }
  
    return {messageResponse: outputMessages, contextReponse: data.queryResult.outputContexts}
}

module.exports = {
    convertJSON
}