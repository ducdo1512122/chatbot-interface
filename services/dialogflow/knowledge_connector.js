const dialogflow = require('dialogflow').v2beta1;
var common_config = require('./configs');

var session_client = new dialogflow.SessionsClient(common_config.config);
var language_code = 'en-US'
var client_id = "123456"

// https://github.com/googleapis/nodejs-dialogflow/blob/master/samples/detect.v2beta1.js#L371

async function create_knowledge_base (displayName) {
    const client = new dialogflow.KnowledgeBasesClient(common_config.config);    

    const formattedParent = client.projectPath(common_config.project_id);
    const knowledgeBase = {
        displayName: displayName,
    };

    const request = {
        parent: formattedParent,
        knowledgeBase: knowledgeBase,
    };

    const [result] = await client.createKnowledgeBase(request);
    console.log(`Name: ${result.name}`);
    return result.name;
}

async function show_knowledge_base() {
    const client = new dialogflow.KnowledgeBasesClient({
        projectPath: common_config.project_id,
    });

    const formattedParent = client.projectPath(common_config.project_id);

    const [resources] = await client.listKnowledgeBases({
        parent: formattedParent,
    });

    resources.forEach(r => {
        console.log(`displayName: ${r.displayName}`);
        console.log(`name: ${r.name}`);
    });
      
}

async function add_document(knowledgeBaseFullName, documentContent, documentName, knowledgeTypes, mimeType) {
    const client = new dialogflow.DocumentsClient(common_config.config, {
        projectId: common_config.project_id,
    });

    const request = {
        parent: knowledgeBaseFullName,
        document: {
          knowledgeTypes: [knowledgeTypes],
          displayName: documentName,
          rawContent: documentContent,
          source: `rawContent`,
          mimeType: mimeType,
        },
    };
    
    const [operation] = await client.createDocument(request);
    const [response] = await operation.promise();
    console.log(`source...${response.source}`);

    return response;
}

async function dectectTextIntentWithKnowledgeBase(query) {
    const sessionPath = session_client.sessionPath(common_config.project_id, client_id);
    const knowbase = new dialogflow.KnowledgeBasesClient();
    const knowledgeBasePath = knowbase.knowledgeBasePath(
        common_config.project_id,
        process.env.KNOWLEDGE_BASE_NAME
    );

    const request = {
        session: sessionPath,
        queryInput: {
          text: {
            text: query,
            languageCode: language_code,
          },
        },
        queryParams: {
          knowledgeBaseNames: [knowledgeBasePath],
        },
      };
    
    const responses = await session_client.detectIntent(request);
    const result = responses[0].queryResult;
    console.log(`Query text: ${result.queryText}`);
    console.log(`Detected Intent: ${result.intent.displayName}`);
    console.log(`Confidence: ${result.intentDetectionConfidence}`);
    console.log(`Query Result: ${result.fulfillmentText}`);
    if (result.knowledgeAnswers && result.knowledgeAnswers.answers) {
    const answers = result.knowledgeAnswers.answers;
    console.log(`There are ${answers.length} answer(s);`);
    answers.forEach(a => {
        console.log(`   answer: ${a.answer}`);
        console.log(`   confidence: ${a.matchConfidence}`);
        console.log(`   match confidence level: ${a.matchConfidenceLevel}`);
    });
    }
}

module.exports = {
    create_knowledge_base,
    add_document,
    dectectTextIntentWithKnowledgeBase
}