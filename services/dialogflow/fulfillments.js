var app = require('express')();
var route = require('./fulfillmentsPost');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var fs = require('fs');

if (process.env.MODE == "server") {
  // Certificate settings
  const privateKey = fs.readFileSync(process.env.PRIVATE_KEY, process.env.ENCODING);
  const certificate = fs.readFileSync(process.env.CERTIFICATE, process.env.ENCODING);
  const ca = fs.readFileSync(process.env.CA, process.env.ENCODING);
  const credentials = { 
    key: privateKey,        
    cert: certificate,        
    ca: ca
  };

  var server = require('https').Server(credentials,app);
}
else {
  var server = require('http').Server(app);
}

function fulfillmentConnect() {
    // App set-up
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());

  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.engine('html', require('ejs').renderFile);

  app.use('/', route);

  server.listen(process.env.FULFILLMENT_PORT, function() {
    console.log(`Fulfillment API running at http://${process.env.HOST_NAME}:${process.env.FULFILLMENT_PORT}/`);
  });
}

function getMethod() {
  app.get('/', function(req, res) {
    return res.send("GET method")
  })
}

module.exports = {
  fulfillmentConnect,
  getMethod
}