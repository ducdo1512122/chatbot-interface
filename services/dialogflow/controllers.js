var dialogflow = require('dialogflow');
var common_config = require('./configs');

// https://medium.com/@tzahi/how-to-setup-dialogflow-v2-authentication-programmatically-with-node-js-b37fa4815d89
// https://github.com/googleapis/google-auth-library-nodejs#json-web-tokens
var session_client = new dialogflow.SessionsClient(common_config.config);
var language_code = 'en-US'
var client_id = "123456"

async function dectectTextIntent(query, contexts) {
    const sessionPath = session_client.sessionPath(common_config.project_id, client_id);

    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
        text: {
            text: query,
            languageCode: language_code,
        },
        },
    };

    if (contexts && contexts.length > 0) {
        request.queryParams = {
        contexts: contexts,
        };
    }

    const responses = await session_client.detectIntent(request);
    return responses[0];
}

module.exports = {
    dectectTextIntent
} 