var fs = require('fs');

var obj = JSON.parse(fs.readFileSync(process.env.DIALOGFLOW_CREDENTIALS, 'utf8'));

config = {
  credentials: {
    private_key: obj.private_key,
    client_email: obj.client_email
  }
}

var project_id = 'newagent-rifpgt'

module.exports = {
    config,
    project_id
}