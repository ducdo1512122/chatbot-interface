var express = require('express');
var axios = require('axios');
var router = express.Router();

/* GET home page. */
router.post('/get-conventions', function(req, res) {
    const movieToSearch =
		req.body.queryResult && req.body.queryResult.parameters && req.body.queryResult.parameters.Api_services
			? req.body.queryResult.parameters.Api_services
            : false;
            
        params = {
            page_number: 1,
            entry_per_page: 2
        }

        axios.post(process.env.WEBHOOK_API, params)
            .then(function (response) {
                res.json({
                    fulfillmentText: JSON.stringify(response.data),
                    fulfillmentMessages: req.body.queryResult.fulfillmentMessages,
                    outputContexts: req.body.queryResult.outputContexts
                })
            })
            .catch(function (error) {
                console.log(error);
            });
});

module.exports = router;