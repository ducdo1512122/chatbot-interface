const Dotenv = require('dotenv-webpack');
const path = require('path');

module.exports = () => ({
    entry: ['./public/js/handleMessage.js'],
    output: {
        path: path.join(__dirname, "public/js"),
        filename: 'handleMessage-webpack.js'
    },
    plugins: [
        new Dotenv()
    ]
})