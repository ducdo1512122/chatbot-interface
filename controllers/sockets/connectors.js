var socket_connector = require('./configs')
var dialogflow = require("../../services/dialogflow/controllers");
var google_speech = require("../../services/google_speech_api/controllers");

var util = require("../../services/utils/utils")

var io = require('socket.io')(socket_connector.socket_server);
var stringConstructor = "string".constructor;

// Google Speech Api will fix later
const speech = require('@google-cloud/speech');
const speechClient = new speech.SpeechClient();

const encoding = 'LINEAR16';
const sampleRateHertz = 16000;
const languageCode = 'en-US'; //en-US

const request = {
    config: {
        encoding: encoding,
        sampleRateHertz: sampleRateHertz,
        languageCode: languageCode,
        profanityFilter: false,
        enableWordTimeOffsets: true,
    },
    interimResults: false // If you want interim results, set this to true
};
var recognizeStream = null;
// ==========================================================================

function init_connection() {
    socket_connector.socket_server.listen(process.env.PORT_SOCKET, function() {
        console.log(`Port socket ${process.env.PORT_SOCKET} is listening`)
    });
}

function run_connection() {
    io.on('connection', function (socket) {
        socket.on('fromClient', function (data) {
            if (data.text.constructor === stringConstructor ) {
                inputText = data.text
            }
            else {
                inputText = data.text.value
            }
      
            dialogflow.dectectTextIntent(inputText, data.context).then( function(res) {
                socket.emit('fromServer', { 
                    server: JSON.stringify(util.convertJSON(res))
                });
            });
        });

        socket.on('startGoogleCloudStream', function (data) {
            startRecognitionStream(this, data);
          });
    
        socket.on('endGoogleCloudStream', function (data) {
            stopRecognitionStream();
        });

        socket.on('binaryData', function (data) {
            if (recognizeStream !== null) {
                recognizeStream.write(data);
            }
        });

        // ======================== GOOGLE SPEECH FUNCTIONS ==========================
        function startRecognitionStream(client, data) {
            recognizeStream = speechClient.streamingRecognize(request)
                .on('error', console.error)
                .on('data', (data) => {
                    
                    client.emit('speechData', data);
        
                    if (data.results[0] && data.results[0].isFinal) {
                        stopRecognitionStream();
                        startRecognitionStream(client);
                    }
                });
        }
        
        function stopRecognitionStream() {
            if (recognizeStream) {
                recognizeStream.end();
            }
            recognizeStream = null;
        }
        // ===========================================================================
    });
}

module.exports = {
    init_connection,
    run_connection
}