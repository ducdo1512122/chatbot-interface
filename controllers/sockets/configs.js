var app = require('express')();

var config = require("../../configs/protocols");

if (process.env.MODE == "server") {
    var socket_server = require('https').Server(config.credentials, app);
}
else {
    var socket_server = require('http').Server(app);
}

module.exports = {
    socket_server
}