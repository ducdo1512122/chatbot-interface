
min = false
max = true

// Functions to open/close chatbot window
function openForm() {
    document.getElementById("chatUI").style.display = "block";
    $("#openBtn").hide();
  }
  
function closeForm() {
    document.getElementById("chatUI").style.display = "none";
    $("#openBtn").show();
} 

function maxForm() {
  if (max == false) {
    $('.chat_window').animate(
      {
        width:'800px',
        top: '50%',
        left: '50%',
      }, 500);
  
    $('.chat_window').css({
      position: 'absolute',
      bottom: 'auto',
      right: 'auto',
    });
    max = true;
    min = false
  }
  $("#expandBtn").hide();
  $("#compressBtn").show();
}

function minForm() {
  if (min == false) {
    $('.chat_window').animate(
      {
        width:'330px',
        bottom: '-15em',
        right: '-10em',
      }, 500);
  
    $('.chat_window').css({
      top: 'auto',
      left: 'auto',
      position: 'fixed',
    });
    max = false
    min = true;
  }
  $("#expandBtn").show();
  $("#compressBtn").hide();
}