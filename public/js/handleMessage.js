if (process.env.MODE == "server") {
    var socket = io("https://" + process.env.HOST_NAME + ':' + process.env.PORT_SOCKET);
}
else {
    var socket = io(process.env.HOST_NAME + ':' + process.env.PORT_SOCKET);
}

//================= CONFIG =================
// Stream Audio
let bufferSize = 2048,
	AudioContext,
	context,
	processor,
	input,
	globalStream;

//vars
let finalWord = false,
	removeLastSentence = true,
	streamStreaming = false;


//audioStream constraints
const constraints = {
	audio: true,
	video: false
};

var temp_context = null;
var recognition = null;
var getMessageText, sendMessage;
var MessageHuman;
var MessageBot;

var checkWebSpeech = false; // if true, use Web Speech Api
var checkDiaglogflow = false; // if true, use Dialogflow Api -- stop development for now
var checkGoogleApi = false;

var recognizing = false;

var checkRecording = false;
var mediaRecorder = null;
var audioChunks = [];

// https://stackoverflow.com/questions/49788422/socket-on-event-gets-triggered-multiple-times
// https://www.bestcssbuttongenerator.com
// https://stackoverflow.com/questions/1570905/use-jquery-to-set-value-of-div-tag

MessageHuman = function (arg) {
    this.response = arg.response, this.message_side = arg.message_side;
    this.draw = function (_this) {
        return function () {
            var $message;
            $message = $($('.message_template').clone().html());

            $message.addClass(_this.message_side).find('.text').html(_this.response);
            $('.messages').append($message);
            setTimeout(function () {
                $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
};

MessageBot = function (arg) {
    this.response = JSON.parse(arg.response.server), this.message_side = arg.message_side;
    temp_context = this.response.contextReponse
    this.draw = function (_this) {
        return function () {

            if (_this.response.messageResponse.text[0]) {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.response.messageResponse.text[0]);
                $('.messages').append($message);
                setTimeout(function () {
                    $message.addClass('appeared');
                }, 0);
            }
            
            if ("image" in _this.response.messageResponse) {
                var $image;
                $image = $($('.image_template').clone().html());

                $image.addClass(_this.message_side);
                $image.attr("src", _this.response.messageResponse.image);

                var $payloads = $($('.payload_template').clone().html());
                $payloads.append($image);

                $('.messages').append($payloads);
                setTimeout(function () {
                    $image.addClass('appeared');
                    $payloads.addClass('appeared');
                }, 0);
            }
        
            if ("card" in _this.response.messageResponse) {
                var $card;
                $card = $($('.card_template').clone().html());

                $card.addClass(_this.message_side).text(_this.response.messageResponse.card.title);
                $card.addClass(_this.message_side).attr('href', _this.response.messageResponse.card.link);
                
                var $payloads = $($('.payload_template').clone().html());
                $payloads.append($card);

                $('.messages').append($payloads);
                setTimeout(function () {
                    $card.addClass('appeared');
                    $payloads.addClass('appeared');
                }, 0);
            }
        
            if ("payload" in _this.response.messageResponse) {
                // Add message of payload
                var $header;
                $header = $($('.message_template').clone().html());
                $header.addClass(_this.message_side).find('.text').html(_this.response.messageResponse.payload.message)

                $('.messages').append($header);
                setTimeout(function () {
                    $header.addClass('appeared');
                }, 0);

                // Add buttons of payload
                var $payloads = $($('.payload_template').clone().html());
                for (item of _this.response.messageResponse.payload.values) {
                    var $payload;
                    $payload = $($('.button_template').clone().html());
                    $payload.text(item.value)

                    $payloads.append($payload);
                    setTimeout(function () {
                        $payload.addClass('appeared');
                    }, 0);
                }

                $('.messages').append($payloads);
                setTimeout(function () {
                    $payloads.addClass('appeared');
                }, 0);
            }
        };
    }(this);
    return this;
};

checkMicroSupport = function () {
    if ('webkitSpeechRecognition' in window) {
        console.log('use web speech api');
        checkWebSpeech = true;
        useSttApi();
        $('.microToggle').attr('onClick', 'useSttApi(event)');
        // to ubind function, use unbind()
    }
    else if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        checkGoogleApi = true;
        // useDialogflowApi();   
    }
    else {
        $('.microToggle').click(function() {
            $(this).toggleClass('fa-microphone fa-microphone-slash');
            $(this).attr("title", "Your browser is not supported microphone. Please check again.");
        })
    }
}

getMessageText = function () {
    var $message_input;
    $message_input = $('.message_input');
    
    return $message_input.val();
};

useSttApi = function () {
    recognition = new webkitSpeechRecognition(); //That is the object that will manage our whole recognition process. 
    recognition.continuous = false;   //Suitable for dictation. 
    recognition.interimResults = false;  //If we want to start receiving results even if they are not final.
    user_mess = ''

    recognition.onstart = function() {
        recognizing = true;

        $('.microToggle').toggleClass('mic-black mic-red');

        if($('.microToggle').hasClass('mic-red')) {
            $('.microToggle').attr("title", "Click again to stop recording...");
        }
    };

    recognition.onerror = function(event) {
        console.log(event.error);
    };

    recognition.onend = function() {
        recognizing = false;

        $('.microToggle').toggleClass('mic-black mic-red');

        if($('.microToggle').hasClass('mic-black')) {
            $('.microToggle').attr("title", "Click to start recording...");
        }
    };

    recognition.onresult = function(event) { //the event holds the results
        if (typeof(event.results) === 'undefined') { //Something is wrong…
            recognition.stop();
            return;
        }
    
        for (var i = event.resultIndex; i < event.results.length; ++i) {      
            if (event.results[i].isFinal) { 
                user_mess = event.results[i][0].transcript; 
            } else {
                user_mess = event.results[i][0].transcript;
            } 
        }

        sendMessage(user_mess);
    };

    //Define some more additional parameters for the recognition:
    recognition.lang = "en-US"; 
    recognition.maxAlternatives = 1; //Since from our experience, the highest result is really the best...
}

useDialogflowApi = function () {
    navigator.mediaDevices.getUserMedia({
        audio: true
    }).then(function(stream) { // Success calback
        mediaRecorder = new MediaRecorder(stream);

        mediaRecorder.addEventListener("dataavailable", event => {
            audioChunks.push(event.data);
        });

        mediaRecorder.addEventListener("stop", () => {
            var reader = new window.FileReader();
            var audioBlob = new Blob(audioChunks, {type:'audio/mpeg-3'});

            reader.readAsDataURL(audioBlob);

            reader.onloadend = function () {
                base64data = reader.result;
                console.log(base64data);
                sendVoice(base64data);
            }
        });

    }).catch(function(err) {
        // console.log('The following getUserMedia error occured: ' + err);
        $('.microToggle').toggleClass('mic-black mic-red');
        $('.microToggle').toggleClass('fa-microphone fa-microphone-slash');
        $('.microToggle').attr("title", "You must reload page to use this function.");
    });
}

// =================== Use Google Cloud Api =========================


//================= RECORDING =================

function initRecording() {
	socket.emit('startGoogleCloudStream', ''); //init socket Google Speech Connection
	streamStreaming = true;
	AudioContext = window.AudioContext || window.webkitAudioContext;
	context = new AudioContext({
		// if Non-interactive, use 'playback' or 'balanced' // https://developer.mozilla.org/en-US/docs/Web/API/AudioContextLatencyCategory
		latencyHint: 'interactive',
	});
	processor = context.createScriptProcessor(bufferSize, 1, 1);
	processor.connect(context.destination);
	context.resume();

	var handleSuccess = function (stream) {
		globalStream = stream;
		input = context.createMediaStreamSource(stream);
		input.connect(processor);

		processor.onaudioprocess = function (e) {
			microphoneProcess(e);
		};
	};

	navigator.mediaDevices.getUserMedia(constraints)
        .then(handleSuccess)
        .catch(function(err) {
            console.log('The following getUserMedia error occured: ' + err);
        });

}

function microphoneProcess(e) {
    var left = e.inputBuffer.getChannelData(0);
    var left16 = downsampleBuffer(left, 44100, 16000)
	socket.emit('binaryData', left16);
}

//==================================================================== 

function startRecording() {
    $('.microToggle').toggleClass('mic-black mic-red');
	initRecording();
}

function stopRecording() {
    $('.microToggle').toggleClass('mic-black mic-red');
    socket.emit('endGoogleCloudStream', '');


	let track = globalStream.getTracks()[0];
	track.stop();

	input.disconnect(processor);
	processor.disconnect(context.destination);
	context.close().then(function () {
		input = null;
		processor = null;
		context = null;
		AudioContext = null;
	});
}

//================= SOCKET IO =================
socket.on('speechData', function (data) {
	sendMessage(data.results[0].alternatives[0].transcript);
	var dataFinal = undefined || data.results[0].isFinal;

	if (dataFinal === false) {
		removeLastSentence = true;

		//add children to empty span
		let edit = data;

		for (var i = 0; i < edit.length; i++) {
            console.log(edit[i])
		}

	} else if (dataFinal === true) {
		//add children to empty span
		let edit = data;
		for (var i = 0; i < edit.length; i++) {
			if (i === 0) {
				edit[i].innerText = capitalize(edit[i].innerText)
			}
			resultText.lastElementChild.appendChild(edit[i]);

			if (i !== edit.length - 1) {
				resultText.lastElementChild.appendChild(document.createTextNode('\u00A0'));
			}
		}

        console.log("Google Speech sent 'final' Sentence.");
		finalWord = true;

		removeLastSentence = false;
	}
});

window.onbeforeunload = function () {
	if (streamStreaming) { socket.emit('endGoogleCloudStream', ''); }
};

//==================================================================== 

getMessageVoice = function (event) {

    // Use Web Speech API
    if (checkWebSpeech == true) {
        if (recognizing) {
           recognition.stop();
           return;
        }

        recognition.start();
    }
    // Use Dialogflow
    else if (checkDiaglogflow == true) {
        var timer;

        if (checkRecording == false) {
            mediaRecorder.start();

            checkRecording = true;
            $('.microToggle').toggleClass('mic-black mic-red');

            if($('.microToggle').hasClass('mic-red')) {
                $('.microToggle').attr("title", "Click again to stop recording...");
            }

            timer = setTimeout(function() {
                if ($('.microToggle').hasClass('mic-red')) {
                    mediaRecorder.stop();

                    checkRecording = false;
                    $('.microToggle').toggleClass('mic-black mic-red');

                    if ($('.microToggle').hasClass('mic-black')) {
                        $('.microToggle').attr("title", "Click to start recording...");
                    } 

                    audioChunks = [];
                }
                
            }, 5000);
        }
        else {
            mediaRecorder.stop();

            checkRecording = false;
            $('.microToggle').toggleClass('mic-black mic-red');

            if($('.microToggle').hasClass('mic-black')) {
                $('.microToggle').attr("title", "Click to start recording...");
            }

            audioChunks = [];
            clearTimeout(timer);
            timer = null;
        }
        
    }
    // Use Google Speech Api
    else {
        startRecording();

        setTimeout(function() {
            stopRecording();
        }, 5000)
    }
}

sendMessage = function (text) {

    if (text.trim() === '') {
        return;
    }

    var $messages, messageHuman, messageBot;
        
    $('.message_input').val('');
    $messages = $('.messages');

    // Show human input
    messageHuman = new MessageHuman({
        response: text,
        message_side: 'right'
    });
    messageHuman.draw();
    $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);

    // Call API
    socket.emit('fromClient', {
        text: text,
        context: temp_context,
    });

    socket.once('fromServer', function(data) {
        // Show bot response
        messageBot = new MessageBot({
            response: data,
            message_side: 'left'
        });
        messageBot.draw();
        $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        
    });
};

sendVoice = function(audio) {
    // Call API
    socket.emit('fromClient', {
        voice: audio,
        context: temp_context,
    });

    socket.once('fromServer', function(data) {
        // Show bot response
        console.log(JSON.parse(data.server))
    });
}

initChatbot = function (text) {
    var $messages, messageBot;
        
    $messages = $('.messages');

    // Call API
    socket.emit('fromClient', {
        text: text,
        context: temp_context,
    });

    socket.once('fromServer', function(data) {
        // Show bot response
        messageBot = new MessageBot({
            response: data,
            message_side: 'left'
        });
        messageBot.draw();
        $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        
    });
};

// Return response
$(document).on('click','.myButton', function(){
    sendMessage($(this).text());
});

$('.send_message').click(function (e) {
    sendMessage(getMessageText());
});

$('.message_input').keyup(function (e) {
    if (e.which === 13) {
        sendMessage(getMessageText());
    }
});

checkMicroSupport();

$('.microToggle').click(function(event) {
    getMessageVoice(event);
});

// Open conversation
initChatbot("welcome");

//================= SANTAS HELPERS =================

var downsampleBuffer = function (buffer, sampleRate, outSampleRate) {
	if (outSampleRate == sampleRate) {
		return buffer;
	}
	if (outSampleRate > sampleRate) {
		throw "downsampling rate show be smaller than original sample rate";
	}
	var sampleRateRatio = sampleRate / outSampleRate;
	var newLength = Math.round(buffer.length / sampleRateRatio);
	var result = new Int16Array(newLength);
	var offsetResult = 0;
	var offsetBuffer = 0;
	while (offsetResult < result.length) {
		var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
		var accum = 0, count = 0;
		for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
			accum += buffer[i];
			count++;
		}

		result[offsetResult] = Math.min(1, accum / count) * 0x7FFF;
		offsetResult++;
		offsetBuffer = nextOffsetBuffer;
	}
	return result.buffer;
}
